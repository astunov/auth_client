import axios from 'axios'
import { browserHistory } from 'react-router'
import { AUTH_USER, UNAUTH_USER, AUTH_ERROR } from './types'

const ROOT = 'http://localhost:9000'

export function signinUser({ email, password }) {
  return dispatch => {
    axios
      .post(`${ROOT}/signin`, { email, password })
      .then(response => {
        dispatch({ type: AUTH_USER })
        localStorage.setItem('token', response.data.token)
        browserHistory.push('/feature')
      })
      .catch(() => {
          dispatch(authError('Bad Login Info'))
      })
  }
}

export function authError(error) {
    return {
        type: AUTH_ERROR,
        payload: error
    }
}

export function signoutUser() {
    localStorage.removeItem('token')
    return {type: UNAUTH_USER}
}