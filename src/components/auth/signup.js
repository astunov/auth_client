import React, { Component } from 'react'
import { reduxForm, Field } from 'redux-form'
import * as actions from '../../actions'

class Signup extends Component {
  render() {
    const { handleSubmit } = this.props
    return (
      <form>
        <fieldset className="form-group">
          <label htmlFor="email">Email:</label>
          <Field
            type="text"
            name="email"
            component="input"
            className="form-control"
          />
        </fieldset>
        <fieldset className="form-group">
          <label htmlFor="email">Email:</label>
          <Field
            type="text"
            name="password"
            component="input"
            className="form-control"
          />
        </fieldset>
        <fieldset className="form-group">
          <label htmlFor="email">Email:</label>
          <Field
            type="text"
            name="passwordConfirm"
            component="input"
            className="form-control"
          />
        </fieldset>
        <button className="btn btn-primary">Sign Up</button>
      </form>
    )
  }
}

export default reduxForm({
  form: 'signup'
})(Signup)
